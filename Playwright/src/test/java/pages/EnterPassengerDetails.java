package pages;

import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.ElementHandle.WaitForSelectorOptions;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.SelectOption;

public class EnterPassengerDetails {
	
	Page page;
	
	
	public EnterPassengerDetails(Page page) {
		this.page=page;
		page.setDefaultTimeout(60000);
	}
	
	public void selectTitle(String index) {
		ElementHandle title=page.waitForSelector("xpath=(//div[@class='travelers']//select)["+index+"]");
		title.scrollIntoViewIfNeeded();
		title.selectOption(new SelectOption().setLabel("Mr"));
	}
	public void enterFirstName(String index) {
		page.waitForSelector("xpath=(//input[@placeholdevalue='First name'])["+index+"]").type("Nidhin");
	}
	public void enterLastName(String index) {
		page.waitForSelector("xpath=(//input[@placeholdevalue='Last name'])["+index+"]").type("Nihdin");
	}
	
	public void enterEmail () {
		page.getByPlaceholder("Email address").type("nidhinmgd10@gmail.com");
	}
	
	public void enterMobile() {
		page.getByPlaceholder("Phone number").type("9847012345");
	}
	
	public void continueBtn() {
		page.waitForSelector("id=btnContinue").click();
		try {
			page.waitForSelector("xpath=//i[text()='Continue']/parent::a").click();
		}catch (Exception e) {
			return;
		}
	}
	
	public void fillPassengerDetails(String index) {
		selectTitle(index);
		enterFirstName(index);
		enterLastName(index);
	}
}
