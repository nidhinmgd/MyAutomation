package pages;

import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.SelectOption;

public class findFlights {
	
	Page page;

	
	public findFlights(Page page) {
		this.page=page;
	}
	
	public void enterFrom(String from) {
		page.waitForSelector("[name='Origin']").type(from);
	}
	
	public void enterDestination(String to) {
		page.waitForSelector("xpath=//input[@name='Destination']").type(to);
	}
	public void clickOneWay() {
		page.click("xpath=//label[contains(text(),'One way')]");
	}
	public void enterFromDate(String fromDate) {
		page.waitForSelector("xpath=//input[@name='StartDate']").click();
		page.waitForSelector("xpath=//li[text()='"+fromDate+"']").click();
	}
	
	public void enterToDate(String toDate) {
		page.waitForSelector("xpath=//input[@name='EndDate']").click();
		page.waitForSelector("xpath=//li[text()='"+toDate+"']").click();
	}
	
	public void SelectAdults() {
		Locator select=page.locator("xpath=//select[@name='AdultsFlight']");
		select.selectOption(new SelectOption().setValue("2"));
	}
	
	public void FindFlightsBtn() {
		page.waitForSelector("xpath=//i[contains(text(),'Find flights')]/parent::a").click();
	}

}
