package pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.testng.Assert;

public class FlightsListPage {
	
	Page page;
	
	public FlightsListPage (Page page) {
		this.page=page;
	}
	
	public void getTitle() {
		String title=page.title();
		System.out.println(title);
		Assert.assertTrue(title.toLowerCase().contains("flights from"));
	}
	
	public void BookPage() {
		Locator bookBtn=page.locator("xpath=//span[text()='Book']/parent::button").first();
		bookBtn.click();
	}

	
}
