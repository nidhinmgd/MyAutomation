package pages;

import org.testng.Assert;
import org.testng.Reporter;

import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.Frame;
import com.microsoft.playwright.FrameLocator;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.SelectOption;

public class payementPage {

	Page page;
	
	public  payementPage(Page page) {
		this.page=page;
	
	}
	
	String cardNo="id=txtCreditCardNumber";
	String nameOnCard="id=txtCreditCardNameAsOnCard";
	String expMonth="id=ddlCreditCardExpiryMonth";
	String expYear="id=ddlCreditCardExpiryYear";
	String cvvNo="id=txtCreditCardCVV";
	String payBtn="id=btnPay";
	String f="xpath=//div[@id='divPaymentPanel']//iframe";
	
	FrameLocator frame;;
	public void enterCardNo() {
		page.waitForLoadState();
//		page.frameLocator(frame).locator(cardNo).fill("6084380648503138");
		frame=page.frameLocator(f);
		frame.locator(cardNo).fill("6074380648503138");
	}
	
	public void enterCardName() {
		frame.locator(nameOnCard).type("Nidhin");
	}
	
	public void enterCvv() {
		frame.locator(cvvNo).type("833");
	}
	
	public void selectExpiry() {
		Locator element =frame.locator(expMonth);
		element.selectOption(new SelectOption().setIndex(1));
		
		element=frame.locator(expYear);
		element.selectOption(new SelectOption().setIndex(2));
	}
	
	public void payBtnClick() {
		frame.locator(payBtn).click();
	}
	
	public void getMsg() throws Exception{
		Thread.sleep(30000);
		ElementHandle element=page.waitForSelector("xpath=//div[@class='itinerary payment']//p");
		Boolean bool=element.isVisible();
		System.out.println(element.textContent());
		System.out.println(bool);
		Reporter.log("success");
		Assert.assertEquals(bool.booleanValue(), true);
		
	}
}
