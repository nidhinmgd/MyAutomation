package tests;

import org.testng.annotations.Test;

import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.Playwright;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class TC_01_SampleApi {
  @Test
  public void f() {
  }
  @BeforeTest
  public void beforeTest() {
	  Playwright playwright=Playwright.create();
	  APIRequest req=playwright.request();
	  APIRequestContext context=req.newContext();
	  APIResponse response=context.get("https://cat-fact.herokuapp.com/facts/");
	  System.out.println(response.text());
  }

  @AfterTest
  public void afterTest() {
  }

}
