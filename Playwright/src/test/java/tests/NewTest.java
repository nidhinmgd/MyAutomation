package tests;

import org.testng.annotations.Test;

import com.microsoft.playwright.Page;
import com.utils.Drivers;

import pages.EnterPassengerDetails;
import pages.FlightsListPage;
import pages.findFlights;
import pages.payementPage;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class NewTest {
	Page page;
  @Test(priority = 0)
  public void BookFlight() throws Exception{
	  findFlights page1=new findFlights(page);
	  page1.clickOneWay();
	  page1.enterFrom("Kochi, India (COK)");
	  page1.enterDestination("Thiruvananthapuram, India (TRV)");
	  page1.enterFromDate("15");
//	  page1.enterToDate("12");
	  page1.SelectAdults();
	  page1.FindFlightsBtn();
	  
	  
	  FlightsListPage page2=new FlightsListPage(page);
	  page2.BookPage();
	  
	  EnterPassengerDetails page3=new EnterPassengerDetails(page);
	  page3.fillPassengerDetails("1");
	  page3.fillPassengerDetails("2");
	  page3.enterEmail();
	  page3.enterMobile();
	  page3.continueBtn();
	  
	  payementPage page4 =new payementPage(page);
	  page4.enterCardNo();
	  page4.enterCardName();
	  page4.enterCvv();
	  page4.selectExpiry();
	  page4.payBtnClick();
	  page4.getMsg();
  }
  
  @BeforeTest
  public void beforeTest() {
	  Drivers d=new Drivers(page);
	  d.browser();
	  page=d.page;
	 
	  
  }

  @AfterTest
  public void afterTest() throws Exception{
	Thread.sleep(10000);
  }

}
