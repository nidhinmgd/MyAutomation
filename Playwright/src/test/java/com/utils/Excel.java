package com.utils;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	FileInputStream ofile;
	XSSFWorkbook oworkbook;
	XSSFSheet osheet;
	XSSFRow orow;
	XSSFCell ocell;
	
	
	public Excel(String filepath , String sheetName) throws Exception {
		 ofile=new FileInputStream(filepath);
		 oworkbook=new XSSFWorkbook(ofile);
		 osheet=oworkbook.getSheet(sheetName);
		
	}
	
	public String getCellData(int rowNo,int cellNo) throws Exception {
		 orow=osheet.getRow(rowNo);
		 ocell=orow.getCell(cellNo);
		 String cellcontent=ocell.getStringCellValue();
		 return cellcontent;
	}





public String getCellData(String filepath , String sheetName,int rowNo,int cellNo) throws Exception {
	FileInputStream ofile=new FileInputStream(filepath);
	XSSFWorkbook oworkbook=new XSSFWorkbook(ofile);
	XSSFSheet osheet=oworkbook.getSheet(sheetName);
	XSSFRow orow=osheet.getRow(rowNo);
	XSSFCell ocell=orow.getCell(cellNo);
	String cellcontent=ocell.getStringCellValue();
	return cellcontent;
}
public int getTotalRowNo() {
	return osheet.getLastRowNum();
}
public Object[][] getExcelforDp() throws Exception{
	orow=osheet.getRow(0);
	Object[][] exceldata;
	int rowno=osheet.getLastRowNum();
	int colno=orow.getLastCellNum();
	exceldata=new Object[rowno][colno];
	for(int i=1;i<=rowno;i++)
	{
		for (int j=0;j<colno;j++)
		{
			exceldata[i-1][j]=this.getCellData(i, j);
		}
	}
	return exceldata;
	
}
}