package com.utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer {
	private static final int maxRetries = 3; // Maximum number of retries
	public static int retryCount = 0;
	private static String methodName;


	@Override
	public boolean retry(ITestResult result) {
		methodName = result.getMethod().getMethodName();
		if (retryCount < maxRetries) {
			retryCount++;
			System.out.println("Retry for method " + methodName + ": " + retryCount);
			return true; // Retry the test method
		}
		return false;
	}

}
